#!/usr/bin/env python
# -*- coding: utf-8 -*-

__date__ = 20210513
__author__ = "Falk Mielke"


#### THIS IS THE CAPTAIN ####
"""
Here, we control one or multiple cameras, attached to raspberry PIrates.
Because this controls the CAPture, it is the Captain.

The purpose of the captain class below is to send commands to different streaming devices
(via a socket), then retrieve their response (hopefully confirmation),
and collects bounty (streams).

Note the "VideSettings" to be set below.

If you choose the GUI captain, here are the commands:
    - Record: start a recording on the Pirate (to file in @pirate:~/funcameracaptain/recordings )
    - View: attempt to stream a video via network (to see through the eyes of the pirate)
    - Stop: stops a stream (view|record)
    - Ping: check if the python socket is active @pirate
    - Wake: start the python socket @piratepirate
    - Kill: quit the python socket program @pirate
    - Grab: retrieve all files from @pirate which are not already in the local ./recordings
    - Del: delete all files which have been grabbed before
    - Reboot: send a reboot command to the pirate
    - Shutdown: send a shutdown command to the pirate



It's great to be a captain.
Note that this clear hierarchical notation (Captain, Pirate) is not intended to harm the feelings
of true pirates. I just like the metaphor. Arrr....

             .------.
           /  ~ ~   \,------.      ______
         ,'  ~ ~ ~  /  (@)   \   ,'      \
       ,'          /`.    ~ ~ \ /         \
     ,'           | ,'\  ~ ~ ~ X     \  \  \
   ,'  ,'          V--<       (       \  \  \
 ,'  ,'               (vv      \/\  \  \  |  |
(__,'  ,'   /         (vv   ""    \  \  | |  |
  (__,'    /   /       vv   " "    \ |  / / /
      \__,'   /  |     vv          / / / / /
          \__/   / |  | \         / /,',','
             \__/\_^  |  \       /,'',','\
                    `-^.__>.____/  ' ,'   \
                            // //---'      |
          ===============(((((((=================
                                     | \ \  \
                                     / |  |  \
                                    / /  / \  \
                                    `.     |   \
                                      `--------'
# http://www.asciiworld.com/-Parrots-.html

sockets code started from:
    https://stackoverflow.com/questions/11352855/communication-between-two-computers-using-python-socket

"""

#### Library Import ####
import os as OS # operation system control
import socket as SOCK # sockets: connecting two python processes (or even via network)
import atexit as EXIT # cleanup when a program is closed
import subprocess as SP # launch background processes
import signal as SG # computer process control
import tkinter as TK # a basic, but reliable gui library
import time as TI # stop watch
import GUITools as GUI # some GUI helpers
import threading as TH # handle multiple processes
import re as RE # Regular Expressions, for string search operations
import datetime as DT # setting date and time on the pirates
import configparser as CONF # configuration for the pirate


### Video Settings
# for manual editing!
def GetVideoSettings():
    # units (to get to milliseconds)
    seconds = 1000
    minutes = 60000

    # duration
    recording_duration = 30 * minutes

    # video
    width = 1024
    height = 768
    fps = 30

    # combine settings
    video_settings = { \
                         't': recording_duration \
                       , 'w': width \
                       , 'h': height \
                       , 'fps': fps \
                       }

    return video_settings

# pirate timeout: the maximum time a pirate may take to perform an action
pirate_timeout = 6.
# (necessary to cancel a command if connection is lost; otherwise captain would get stuck)


###########################
#### The Captain Class ####
class Captain(dict):
    # There are many different captains out there.
    # All of them shout out commands and expect their pirates to send bounty.
    # This one is no different.
    #
    # This captain class is a blueprint, i.e. the abstract plan of a captain.
    # Whenever you say "Captain()", a captain is born.


    def __init__(self, captain_address, pirates, run_immediately = True):
        # initializes the captain.
        # input:
        #   captain_address:   a tuple of (ip, port), default port is 4005.
        #   pirate_address:    another such tuple, default port is 4000.

        # this is where you find the captain. The bridge, so to say.
        # captain_ip='192.168..' # Listening (=captain) address
        # captain_port = 4005 # port to LISTEN for camera correspondence
        self.address = captain_address

        # and here is the address of a pirate.
        # IP may be the same, but the port is different!
        self.pirates = pirates

        # establish a connection to the pirates.
        self.ear = SOCK.socket(SOCK.AF_INET, SOCK.SOCK_DGRAM)
        self.ear.bind(self.address)

        # when strange stuff happens, e.g. captain gets drunk, you want to safely close the socket.
        EXIT.register(self.Quit)

        # connect commands to callbacke
        self.SetCommands()

        # life check
        self.is_alive = True

        # optional: extra output
        self.verbose = True

        self.bounty = None # placeholder for a stream process.

        # here, the main loop starts (i.e. automatically, whenever a captain is born).
        if run_immediately:
            self.MainLoop()


    def SetCommands(self):
        ## bind commands
        # these are commands, or "callbacks", which the pirate can receive.
        # The commands come as a string (e.g. 'stop') and execute a function (e.g. Stop).
        # Note that every one of these functions must return a string, which is then reported to the Captain.
        commands = {    'noop': self.NOOP \
                      , 'stream': self.ReceiveStream \
                      , 'record': self.Record \
                      , 'mkv': self.ConvertVideos \
                      , 'bounty': self.Bounty \
                      , 'delete': self.DeleteRecs \
                      , 'stop': self.Stop \
                      , 'view': self.ViewStream \
                      , 'nettest': self.NetworkTest \
                      , 'test': self.Test \
                      , 'ping': self.Ping \
                      , 'wake': self.Wake \
                      , 'shut': self.ShutdownPi \
                      , 'boot': self.RebootPi \
                      , 'kill': self.QuitPi \
                    }

        # all these commands are added to the camera's repertoire.
        for cmd, function in commands.items():
            # the camera
            self[cmd] = function


    def StdOut(self, *args, **kwargs):
        # this is where status messages are directed.
        print (*args, **kwargs)


    def Test(self, pirate, evt = None):
        # invoke a "test" on a RasPirate, which will store a test video to its disk.
        # note: the "evt" keyword is used for all callback functions which might be reached via hotkey.

        # check if there is a connection:
        if not self.Ping(pirate):
            self.StdOut(f'{pirate} not connected')
            return

        self.SendMessage(pirate, 'test')


    def NetworkTest(self, pirate, evt = None):
        # a network stream test.

        # check if there is a connection:
        if not self.Ping(pirate):
            self.StdOut(f'{pirate} not connected')
            return

        # A test video from the RasPirate will be viewed.
        receive_command = """gst-launch-1.0 -v udpsrc port=5000 ! "application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96" ! rtph264depay ! h264parse ! decodebin ! autovideosink sync=false"""
        self.bounty = SP.Popen(receive_command \
                                , stdout=SP.DEVNULL \
                                , stderr=SP.DEVNULL \
                                , shell = True \
                                )
        self.SendMessage(pirate, 'nettest')

    def Record(self, pirate, evt = None):
        # start recording to file on a RasPirate, which will store a video to its disk.
        # note: the "evt" keyword is used for all callback functions which might be reached via hotkey.

        # check if there is a connection:
        if not self.Ping(pirate):
            self.StdOut(f'{pirate} not connected')
            return

        # tell pirate to record
        self.SendMessage(pirate, 'record')


    def ConvertVideos(self, pirate, evt = None):
        # convert all videos on a pirate to "mkv" format

        # check if there is a connection:
        if not self.Ping(pirate):
            self.StdOut(f'{pirate} not connected')
            return

        # tell pirate to record
        self.SendMessage(pirate, 'mkv')

    def матрёшка(self):
        # convert all recordings/timestamps to MKV
        basedir = './recordings'

        # get all files
        files_to_convert = list(sorted([OS.path.splitext(fi)[0] \
                                        for fi in OS.listdir(basedir) \
                                        if OS.path.splitext(fi)[-1] == '.h264' \
                                        ]))

        for nr, fi in enumerate(files_to_convert):
            self.StdOut(f'converting {nr}/{len(files_to_convert)}: {fi}')
            filepath = f"{basedir}/{fi}" # no extension

            # sort timestamps # https://stackoverflow.com/a/22281855
            command = f"cat {filepath}.txt " \
                    + """ | awk 'NR<2{print $0;next}{print | "sort -n"}' """ \
                    + f" > {filepath}.pts"
            OS.system(command)

            
            # prepare command
            command = " && ".join( [ \
                                     f"mkvmerge -o {filepath}.mkv --timecodes 0:{filepath}.txt {filepath}.h264" \
                                   , f"rm {filepath}.h264 {filepath}.txt {filepath}.pts" \
                                    ])

            # execute
            OS.system(command)

        self.StdOut(f'conversion done!')



    def ListRemoteFiles(self, ip):
        # list files in the pirate's "recordings"
        files_remote = OS.popen( \
                                 f"""sshpass -f ".funcam.pwd" ssh morpho@{ip} -f 'ls /home/morpho/funcameracaptain/recordings'""" \
                                ).read()
        files_remote = RE.split('\s|\n', files_remote)

        return files_remote



    def Bounty(self, pirate, evt = None):
        # transfer all pirate bounty to captain
        # will grab all files from "recording/" and move them here.

        # get pirate IP
        ip, _ = self.pirates[pirate]

        # get file lists
        files_local = OS.listdir('recordings') + ['']
        files_remote = self.ListRemoteFiles(ip)

        for grabfile in [fi for fi in files_remote if fi not in files_local]:
            command = f"""sshpass -f ".funcam.pwd" scp morpho@{ip}:/home/morpho/funcameracaptain/recordings/{grabfile} ./recordings/"""
            # execute: grab all files
            OS.system(command)

        self.StdOut(f"{pirate} bounty grabbed!")
        # sshpass -p "password"
        # https://stackoverflow.com/a/13955428

        # TODO: windows testing


    def DeleteRecs(self, pirate, evt = None):
        # empty the "recordings" folder on the remote machine
        # only deleting the files which were already grabbed.

        if not GUI.Prompt( f'Treasure chest of {pirate}' \
                         , f"Arrrr... you sure to plunder {pirate}'s bounty?" \
                         , icon = 'warning' \
                         ):
            return

        # get pirate IP
        ip, _ = self.pirates[pirate]

        # get file lists
        files_local = OS.listdir('recordings') + ['']
        files_remote = self.ListRemoteFiles(ip)
        # IMPORTANT: make sure file names are always unique per pirate

        # previously grabbed files
        rmfiles = " ".join([fi for fi in files_local if fi in files_remote])
        if len(rmfiles) == 0:
            self.StdOut(f"{pirate} has no bounty!")
            return

        # remove already grabbed files
        command = f"""sshpass -f ".funcam.pwd" """ \
            + f"""ssh morpho@{ip} -f "cd /home/morpho/funcameracaptain/recordings """ \
            + f""" && rm {rmfiles}" """

        OS.system(command)
        self.StdOut(f"{pirate} treasure chest emptied!")


    def GenerateSettingsFile(self, pirate):
        # this prepares a settings file using confparser.
        # Content are:
        #   - ip addresses
        #   - video settings
        # the file is stored locally, but can be sent to a pirate.

        # IP addresses
        config = CONF.ConfigParser()
        config['main'] = { \
                               'captain': self.address[0] \
                             , 'pirate': self.pirates[pirate][0] \
                             , 'label': pirate \
                             , 'timeout': pirate_timeout \
                            }

        # video settings
        config['video'] = GetVideoSettings()

        # store config
        with open('.pirate.conf', 'w') as configfile:
            config.write(configfile)


    def PirateStartup(self, pirate):
        # Some Commands which have to be done on wakeup of the pirates
        # Those are:
        #       - sync watches
        #       - tell them who's their captain
        #       - store the address of the pirate

        # check if there is a connection:
        if self.Ping(pirate):
            self.StdOut(f'{pirate} already running.')
            return

        # get pirate IP
        ip, _ = self.pirates[pirate]

        ### sync time
        # set time
        command = f"""sshpass -f ".funcam.pwd" """ \
            + f"""ssh morpho@{ip} -f "sudo date --set '""" \
            + DT.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f") \
            + """'" """

        # print(command)
        OS.system(command)

        ### Prepare a Settings File
        self.GenerateSettingsFile(pirate)

        ### Copy settings to the pirate
        command = f"""sshpass -f ".funcam.pwd" """ \
            + f"""scp .pirate.conf morpho@{ip}:/home/morpho/funcameracaptain/ """

        # print(command)
        if ip == self.address[0]:
            # testing on a single machine
            pass
        else:
            OS.system(command)


    def Wake(self, pirate, evt = None):
        # will start the client program on a pirate

        self.PirateStartup(pirate)

        # prepare wakeup command
        ip, _ = self.pirates[pirate]
        command = f"""sshpass -f ".funcam.pwd" ssh morpho@{ip} -f 'cd /home/morpho/funcameracaptain && python3 FunPirate.py' """

        # print (command)
        OS.system(command)

        self.gui.after(3000, self.Ping, pirate) # wait for pirate to wake up
        # check if there is a connection:
        # if self.Ping(pirate):
        #     self.StdOut(f'{pirate} connected!')
        #     return


    def ShutdownPi(self, pirate, evt = None):
        # will start the client program on a pirate

        # confirm
        if not GUI.Prompt( f'Bury the {pirate}!' \
                         , f"Arrrr... you sure to shutdown {pirate}?" \
                         , icon = 'warning' \
                         ):
            return

        # prepare wakeup command
        ip, _ = self.pirates[pirate]
        command = f"""sshpass -f ".funcam.pwd" ssh morpho@{ip} -f 'sudo shutdown -h now' """

        print(command)
        OS.system(command)

        self.StdOut(f'{pirate} shut down.')
        self.menubars[pirate].Disconnected()


    def RebootPi(self, pirate, evt = None):
        # will start the client program on a pirate

        # prepare wakeup command
        ip, _ = self.pirates[pirate]
        command = f"""sshpass -f ".funcam.pwd" ssh morpho@{ip} -f 'sudo shutdown -r now' """

        print(command)
        OS.system(command)

        self.StdOut(f'{pirate} sent for reboot.')
        self.menubars[pirate].Disconnected()

    def ReceiveStream(self, pirate, target_file = 'testing/test_captain.mov', evt = None):
        # receive a stream to file.

        # check if there is a connection:
        if not self.Ping(pirate):
            self.StdOut(f'{pirate} not connected')

        # this is just a quick and dirty function to receive a stream.
        # TODO: It will be improved. See the Pirates for more info on the GStreamer structure.
        receive_command = f""" gst-launch-1.0 -v udpsrc port=5000 ! "application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96" ! rtph264depay ! h264parse ! decodebin ! x264enc tune=zerolatency quantizer=20 speed-preset=slow ! qtmux ! filesink location={target_file} sync=false -e """

        # this runs the recipient stream via a subprocess.
        self.bounty = SP.Popen(receive_command \
                                , stdout=SP.DEVNULL \
                                , stderr=SP.DEVNULL \
                                , shell = True \
                                )
        self.SendMessage(pirate, 'stream')


    def ViewStream(self, pirate, evt = None):
        # receive a stream to view

        # check if there is a connection:
        if not self.Ping(pirate):
            self.StdOut(f'{pirate} not connected')
            return

        # instead to streaming to a file, the captain may want to view what to expect as bounty.
        receive_command = """gst-launch-1.0 -v udpsrc port=5000 ! "application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96" ! rtph264depay ! h264parse ! decodebin ! autovideosink sync=false"""
        self.bounty = SP.Popen(receive_command \
                                , stdout=SP.DEVNULL \
                                , stderr=SP.DEVNULL \
                                , shell = True \
                                )
        self.SendMessage(pirate, 'view')


    def Stop(self, pirate, evt = None):
        # this stops the current stream.
        # My captain computer was powerful enough so that the whole killing business (see pirates)
        # is unnecessary.
        # Who would want to kill a captain anyways?

        # check if there is a connection:
        if not self.Ping(pirate):
            self.StdOut(f'{pirate} not connected')
            return

        # stop stream
        if self.bounty is not None:
            self.bounty.send_signal(SG.SIGINT)
            self.bounty.wait(10.0)
        self.SendMessage(pirate, 'stop')


    def NOOP(self, pirate, evt = None):
        # Do Nothing. (good for testing)

        # check if there is a connection:
        if not self.Ping(pirate):
            self.StdOut(f'{pirate} not connected')
            return

        # send noop signal
        self.SendMessage(pirate, 'noop')

    def Ping(self, pirate, evt = None):
        # checks whether the pirate is alive.
        # this will be required to check whether a pirate is connected.

        self.pirate_ping = ':/' # unclear situation.
        self.menubars[pirate].Disconnected() # indicate inactive connection

        # pinging is done in a thread.
        # It might be that the ping is never returned, which is problematic. so we set a timeout.
        # https://www.kite.com/python/answers/how-to-set-a-timeout-on-a-socket-receiving-data-in-python
        # https://stackoverflow.com/questions/34371096/how-to-use-python-socket-settimeout-properly
        message = 'ping'.encode('utf-8') # it needs to be in pirate language (encoding).
        pirate_addr = self.pirates[pirate]
        try:
            self.ear.sendto(message, pirate_addr) # here is the shout out to the pirates.
        except OSError as ose:
            # this error appears if the network is disconnected
            self.menubars[pirate].Disconnected()
            return False

        # wait for confirmation from the pirates.
        self.ear.settimeout(1.)
        try:
            data, addr = self.ear.recvfrom(1024)
            self.pirate_ping = data.decode('utf-8')
        except SOCK.timeout as to:
            self.StdOut(f'ping {pirate} failed.')
            self.pirate_ping = ':('

        # de-activate timeout (starting streams takes longer)
        self.ear.settimeout(None)

        # return whether the answer was positive.
        response = self.pirate_ping == ':)'

        if response:
            # change menu bar indicator
            self.menubars[pirate].Connected()

        return response


    def QuitPi(self, pirate, evt = None):
        # kill a pirate

        # check if there is a connection:
        if not self.Ping(pirate):
            self.StdOut(f'{pirate} not connected')
            return

        self.SendMessage(pirate, 'q') # send command to quit
        self.menubars[pirate].Disconnected() # indicate inactive connection


    def MainLoop(self):
        # the main procedure! Repeated until dead mans sit on a chest with a bottle of rum.
        while True:
            # request user input
            message = input("-> ")

            # split message: pirate label first, then command
            message = message.split(' ')

            # set active pirate
            if message[0] not in self.pirates.keys():
                self.StdOut ('pirate not found. Please address one of the pirates:', self.pirates.keys())
                continue

            # get the addressed pirate
            pirate = message[0]

            # get the rest of the message
            message = ' '.join(message[1:])

            # if bounty is to be expected, open an input stream.
            if message in self.keys():
                self[message](pirate)
            else:
                self.SendMessage(pirate, message)

            # finally, if the order was to quit, call the suicide function.
            if message.decode('utf-8') == 'q':
                break

    def SendMessage(self, pirate, message):
        # This sends commands to a pirate of choice.

        # first, select the active pirate
        pirate_addr = self.pirates[pirate]

        # then, send the command to the pirate!
        message = message.encode('utf-8') # it needs to be in pirate language (encoding).
        self.ear.sendto(message, pirate_addr) # here is the shout out to the pirates.

        # wait for confirmation from the pirates.
        # Note: if a new method of the pirates does not return a confirmation,
        # the captain will get stuck here.
        data, addr = self.ear.recvfrom(1024)

        # un-translate from pirate language.
        data = data.decode('utf-8')
        if self.verbose:
                self.StdOut(f"answer from {pirate}: {data}")

        return data


    def Quit(self):
        # this ends the captain (closes the socket)
        for pirate_addr in self.pirates.values():
            self.ear.sendto('q'.encode('utf-8'), pirate_addr)
        self.ear.close()





class GUICaptain(Captain):
    # the drunk version of a captain.
    # Can do all that a captain can, but it has a GUI.

    def __init__(self, captain_address, pirates):
        # initializes the captain - with a GUI!
        # input:
        #   captain_address:   a tuple of (ip, port), default port is 4005.
        #   pirate_address:    another such tuple, default port is 4000.


        ## GUI preparation
        self.gui = TK.Tk()
        self.gui.wm_title('Camera Captain Observer GUI')
        self.gui.configure(bg = GUI.colors['bg'])

        self.pirates = pirates
        self.menubars = {}


        # Trick to add the pirate identifier to the callback
        # the callback functions must get the "pirate" keyword first,
        # make sure that pirate matches a label.
        # https://stackoverflow.com/a/15510153
        def AddressedCallback(cmd, lab):
            # this is a neat trick. Try to wrap your head around it!
            def Wrapper(command = cmd, pirate = lab, **kwargs):
                return self[command](pirate = pirate, **kwargs)
            return Wrapper


        # create menu bar for each pirate
        for label, pirate in self.pirates.items():

            self.menubars[label] = GUI.ToolBar( \
                                  label = label \
                                , buttons = [ \
                                              ['Shutdown', AddressedCallback('shut', label)] \
                                            , ['Reboot', AddressedCallback('boot', label)] \
                                            , ['Del', AddressedCallback('delete', label)] \
                                            , ['Grab', AddressedCallback('bounty', label)] \
                                            , ['Kill', AddressedCallback('kill', label)] \
                                            # , ['Noop', AddressedCallback('noop', label)] \
                                            , ['Wake', AddressedCallback('wake', label)] \
                                            , ['Ping', AddressedCallback('ping', label)] \
                                            #, ['MKV', AddressedCallback('mkv', label)] \
                                            , ['Stop', AddressedCallback('stop', label)] \
                                            , ['View', AddressedCallback('view', label)] \
                                              # , ['Net Test', self.NetworkTest] \
                                            , ['Record', AddressedCallback('record', label)] \
                                          ] \
                                , separators_after = ['Reboot','Grab','Ping'] \
                                , orient_horz = True \
                                , master = self.gui \
                                , relief = TK.FLAT \
                                , borderwidth = 1 \
                                , bg = GUI.colors['bg'] \
                                )
            self.menubars[label].pack(side = 'top')

        # status text (instead of console)
        self.status = TK.StringVar()
        self.status.set('Welcome, Captain!')
        self.indicator_text = TK.Label( \
                                        master = self.gui \
                                        , textvariable = self.status \
                              , bg = GUI.colors['bg'] \
                              , fg = GUI.colors['fg'] \
                              , highlightbackground = GUI.colors['hl'] \
                              , highlightcolor = GUI.colors['fg'] \
                              , borderwidth = 0 \
                              , height = 4 \
                                       )
        self.indicator_text.pack(side = 'top')

        # add a menu for the captain
        self.menu = GUI.ToolBar( \
                                  label = 'master' \
                                , buttons = [ \
                                              ['Quit', self.Quit] \
                                            , ['MKV', self.матрёшка] \
                                            , ['GrabAll', self.GrabAll] \
                                            , ['KillAll', self.QuitPis] \
                                            , ['WakeAll', self.WakeAll] \
                                            , ['PingAll', self.PingAll] \
                                            , ['Stop', self.StopAll] \
                                            , ['Record', self.RecordAll] \
                                          ] \
                                , orient_horz = True \
                                , master = self.gui \
                                , relief = TK.FLAT \
                                , borderwidth = 1 \
                                , bg = GUI.colors['bg'] \
                                )
        self.menu.pack(side = 'top')
        self.menu.Connected()

        # keyboard shortcuts ("hotkeys")
        self.gui.bind('r', self.RecordAll)
        self.gui.bind('s', self.StopAll)
        self.gui.bind('<space>', self.RecordAll)
        self.gui.bind('q', self.QuitPis)
        self.gui.bind('<Escape>', self.Quit)


        super(GUICaptain, self).__init__(captain_address, pirates \
                                         , run_immediately = False)

        # here, the main loop starts (i.e. automatically, whenever a captain is born).
        # self.TestProcedure(pirate)
        self.PingAll() # count pirates
        self.gui.mainloop()


    def StdOut(self, *args, **kwargs):
        # redirect status to status bar
        self.status.set(" | ".join(map(str, args)))
        print (*args, **kwargs)


    def PingAll(self, evt = None):
        # ping all available PIs.
        for pirate in self.pirates.keys():
            self.Ping(pirate)

    def WakeAll(self, evt = None):
        # ping all available PIs.
        for pirate in self.pirates.keys():
            self.Wake(pirate)

    def GrabAll(self, evt = None):
        # ping all available PIs.
        for pirate in self.pirates.keys():
            self.Bounty(pirate)

    def RecordAll(self, evt = None):
        # record on all available pirates.
        for pirate in self.pirates.keys():
            self.Record(pirate)

        self.gui.bind('<space>', self.StopAll)

    def StopAll(self, evt = None):
        # record on all available pirates.
        for pirate in self.pirates.keys():
            self.Stop(pirate)

        self.gui.bind('<space>', self.RecordAll)


    def QuitPis(self, evt = None):
        # kill all available pirates.
        for pirate in self.pirates.keys():
            self.QuitPi(pirate)


    def TestProcedure(self, pirate):
        # the main loop of this captain differs.

        pirate_addr = self.pirates[pirate]

        TI.sleep(1.)
        message = 'noop'.encode('utf-8')
        self.ear.sendto(message, pirate_addr) # here is the shout out to the pirates.

        data, addr = self.ear.recvfrom(1024)
         # un-translate from pirate language.
        data = data.decode('utf-8')
        self.StdOut(f"answer from {pirate}: {data}")

        TI.sleep(3.)

        self.Quit()


    def Quit(self, pirate = None, evt = None):
        # this ends the captain (closes the socket)
        # 'pirate' argument is not used in this case

        """ obsolete: kill all pirates upon closing
        if self.is_alive:
            for pirate_addr in self.pirates.values():
                # run for your lives!
                self.ear.sendto('q'.encode('utf-8'), pirate_addr)

            # the captain won't listen.
            self.ear.close()
        TI.sleep(1.)

        """

        self.gui.quit()     # stops mainloop
        try:
            self.gui.destroy()  # this is necessary on Windows to prevent
                            #   Fatal Python Error: PyEval_RestoreThread: NULL tstate
        except TK.TclError:
            pass

        # dead end.
        self.is_alive = False


def Testing():
    # Using the same computer for captain and pirate.
    captain_address = ('192.168.247.35', 4005)
    pirates = {'pi1': ('192.168.247.35', 4000)}


    import FunPirate as FP
    pirate_jobs = []
    for label, pirate_addr in pirates.items():
        pirate_jobs.append( \
                TH.Thread(target = FP.Pirate ) \
                          )

    for pir in pirate_jobs:
        pir.start()

    # a captain is created to start work.
    GUICaptain(captain_address, pirates)

    for pir in pirate_jobs:
        pir.join()


#########################
#### Mission Control ####
if __name__== '__main__':
    # if this is the main script that is run (e.g. by calling "python CaptainObserver.py"),
    # then this will start testing by default.

    Testing()


# HOLD: test whether the gstreamer timestamps are accurate, otherwise log start and stop commands. Is there a "timestamps" output like in raspivid?
