#!/usr/bin/env python
# -*- coding: utf-8 -*-

__date__ = 20210513
__author__ = "Falk Mielke"


#### THIS IS THE CAMERA PIRATE ####
"""
In this part of the code, we control the "pirates",
i.e. the Pi computers with camera attached.

They run a socket and listen fro commands from a central machine (the Captain).
There are different methods of streaming video material to the Captain.

Currently, this uses "raspivid" on raspberry pi clients; but output method is easy to adjust.
Set the video settings below!


sockets code started from:
    https://stackoverflow.com/questions/11352855/communication-between-two-computers-using-python-socket
subprocess:
    https://stackoverflow.com/questions/12605498/how-to-use-subprocess-popen-python
"""

#### Library Import ####
import os as OS # operating system control
import socket as SOCK # sockets: connecting via the network
import atexit as EXIT # cleanup when the program is closed
import subprocess as SP # launch processes in the background, while this script stays active
import signal as SG # signals between computer processes
import time as TI # time
import datetime as DT # to give the recording a timestamp
import logging as LOG # log output to file
import configparser as CONF # read the config file

NowStr = lambda: DT.datetime.now().strftime("%Y%m%d_%H%M%S")






##########################
#### The Pirate Class ####
# Camera Control
class Pirate(dict):
    # this controls the side of the camera unit.
    # Note that the Pirate is a subclass of "dict", so it has all the functions of a python dictionary, plus more
    # Keys/values are used to store the commands and functions that the unit can handle.
    # Main components of a camera unit:
    #   - a socket, to receive commands from the captain and respond
    #   - functions to control video streams (via gstreamer, as a subprocess)

    def __init__(self):
        # initializes the camera control.
        #
        # inputs:
        #   pirate_address      a tuple of (ip, port) that refers to the remote CAMERA unit (i.e. the pirate), NOT the "captain".
        #   timeout             some processes take longer on the RasPi; a longer timeout prevents killing of innocent (slow) processes.

        # load all necessary info from a config
        self.LoadConfig()

        # this variable is saved as part of every Pirate instance.
        self.label = self.config['main']['label'] # give the camera a name

        # the pirate's IP
        self.pirate_address = (self.config['main']['pirate'], 4000)

        # the link to the captain (where net streams go)
        self.captain_address = (self.config['main']['captain'], 4005)
        self.timeout = self.config['main']['timeout'] # also stored for use within the other class methods (=functions).

        # create and configure logger
        LOG.basicConfig(filename = f"./recordings/{NowStr()}_{self.label}.log",
                        format = '%(asctime)s %(message)s',
                        filemode = 'a' \
                        )
        self.log = LOG.getLogger()
        self.log.setLevel(LOG.DEBUG)

        # establish the socket connection
        self.ConnectSocket()

        # if a particular Pirate is destroyed (e.g. by a crash), the "quit" function is called nevertheless.
        EXIT.register(self.Quit)


        ## bind commands
        # these are commands, or "callbacks", which the pirate can receive.
        # The commands com as a string (e.g. 'stop') and execute a function (e.g. Stop).
        # Note that every one of these functions must return a string, which is then reported to the Captain.
        commands = {  'noop': self.NOOP \
                    , 'test': self.TestStream \
                    , 'nettest': self.NetworkTestStream \
                    , 'c2f': self.Cam2FileStream \
                    , 'stream': self.Cam2UDPStream \
                    , 'record': self.Raspivid2File \
                    , 'view': self.Cam2UDPStream \
                    , 'stop': self.Stop \
                    , 'mkv': self.матрёшка \
                    , 'ping': self.Ping \
                    }

        # all these commands are added to the camera's repertoire.
        for cmd, function in commands.items():
            # the camera
            self[cmd] = function

        # some variables to keep track of the camera activity
        self.recording_counter = 0 # count recordings
        self.is_alive = True # set to "false" when this unit gets useless
        self.busy = False # keep track of activity state
        self.stream = None # just a placeholder to store the stream process


        # START the main procedure
        self.MainLoop()


#### Pirate Methods ####
# the ways of a pirate.
    def StdOut(self, *args, **kwargs):
        # this function can be used to redirect relevant responses later, e.g. via a logger to the captain's log book.
        # print (*args, **kwargs)
        self.log.info(*args, **kwargs)


    def NOOP(self):
        # do nothing (used for testing)
        return 'did nothing.'

    def LoadConfig(self):
        # the captain's commands are stored in a config file.
        # this function reads them.
        # https://docs.python.org/3/library/configparser.html

        # read the content of the config file
        config = CONF.ConfigParser()
        config.read('.pirate.conf')

        # all config values are "string" per default.
        # We will convert some below.
        dtypes = { \
                   'timeout': float \
                   , 't': int \
                   , 'w': int \
                   , 'h': int \
                   , 'fps': float \
                  }

        # store config content in a dict
        self.config = {}
        # loop categories
        for cat, settings in config.items():
            self.config[cat] = {}
            # loop settings
            for key, value in settings.items():
                # optionally convert data type
                self.config[cat][key] = dtypes.get(key, str)(value)



    def ConnectSocket(self):
        # this function establishes the python network socket.

        self.ear = SOCK.socket(SOCK.AF_INET, SOCK.SOCK_DGRAM) # got this from StackOverflow (was my first socket).
        self.ear.bind(self.pirate_address) # the socket is bound to listen on this unit's IP/Port.

        self.StdOut("socket up!") # confirm successful connection (mental note: it might be better to actually check the status of the socket)


    def MainLoop(self):
        # the main procedure:
        #    await orders, make bounty, kill soldiers, capture mermaids, repeat

        # As long as it is not dead...
        while self.is_alive:
            # ... the pirate is permanently ready for commands by the captain.
            command, captain = self.ear.recvfrom(1024) # this will halt and wait until orders are received.

            # commands have to be decoded.
            command = command.decode('utf-8')

            # the message is repeated, so that the captain knows we understood the order.
            self.StdOut(f"Message from {str(captain)}: {command}")

            # a special order: the "quit" command.
            if command == 'q':
                # inform the captain about imminent suicide
                self.ReturnToSender(captain, f'{self.label} exiting.')

                # break the main loop, thereby exiting.
                break

            # another command was issued
            if command in self.keys():
                # immediately execute the order
                result = self[command]()
                # ... and tell the boss what came out.
                self.ReturnToSender(captain, result)

            else:
                # the command was not understood, which we tell the boss.
                self.ReturnToSender(captain, 'command not found.')


    def ReturnToSender(self, addr, result):
        # this uses the socket to report back to the captain (or any other address)
        # strings have to be encoded for transmission.
        self.ear.sendto(result.encode('utf-8'), addr)

    def Ping(self):
        # check if this pirate is alive.
        return ':)' if self.is_alive else ':('

    def Quit(self):
        # exit elegantly

        if not self.is_alive:
            # ...oops?! someone killed this pirate before.
            self.StdOut(f'{self.label} dead already.')
            return

        # if it was busy, stop that activity first.
        if self.busy:
            self.Stop()

        # now it's dead.
        self.is_alive = False
        self.ear.close()# gently close the socket.

        # ...and give feedback.
        self.StdOut(f'{self.label} shut down.')

#### Streaming ####
# The following functions facilitate issuing a gstreamer command.
# StartStream concatenates the stream parameters and returns the bounty.
# The other functions are for storing different stream parameters for different purposes.

    def StartStream(self, stream_pipeline):
        # get the bounty home: here we start a stream.
        # Because the pirate is busy listening (and has to stay aware of captain's orders), the stream will be a subprocess.

        if self.busy:
            # if this pirate is busy, nothing is done.
            # yet at least we have to tell the captain, don't we?
            return f'{self.label} still busy!'


        # This will start the subprocess.
        self.stream = SP.Popen( stream_pipeline, shell = True)

        # Some mis-configured streams fail immediately.
        # And stream initialization takes some time.
        # So let's wait a few seconds (timeout) and check.
        TI.sleep(self.timeout)# need to wait a bit
        # the stream.poll() function returns "None" if the subprocess is busy.
        if self.stream.poll() is not None:
            # so if it is "not None", the stream is over already or did not start.
            self.Stop() # so better clean up and report!
            return f'{self.label} stream failed!'

        # raise the "busy" flag
        self.busy = True

        # and report that bounty is flowing.
        return f'{self.label} stream started.'


    def ConcatenateGStreamerCommand(self, vid_source, vid_target, vid_settings, enc_params):
        # concatenates a complex GSt command and runs the stream

        # Gstreamer works with "pipelines".
        # These direct video data from a sorce, through encoders and muxers, to a destination (network, file, etc.)
        # Components are separated by the gstreamer pipe symbol: '!'.
        # Here, we combine the command.
        return self.StartStream(stream_pipeline = \
                         ' ! '.join( [ \
                                vid_source \
                              , f'video/x-raw,{vid_settings}' \
                              , f'x264enc {enc_params}' \
                              , vid_target \
                              ] ) \
                         )



    def TestStream(self):
        # STEP 1a:
        # this stream is configured for testing local storage.
        # It streams a test video to a local testing file.
        vid_source = 'gst-launch-1.0 -e -v videotestsrc' # source; '-e -v' gives some extra log output.
        vid_settings = 'width=640,height=480,framerate=5/1' # settings for 'video/x-raw'
        enc_params = 'tune=zerolatency quantizer=30 speed-preset=fast' # encoding parameters
        vid_target = 'qtmux ! filesink location=testing/test_pirate.mov sync=false -e' # target of the video stream (a file)
        return self.ConcatenateGStreamerCommand(vid_source, vid_target, vid_settings, enc_params)

    def NetworkTestStream(self):
        # STEP 1b:
        # this stream is configured for testing network stream.
        # It streams a test video to a local testing file.
        vid_source = 'gst-launch-1.0 -e -v videotestsrc' # source; '-e -v' gives some extra log output.
        vid_settings = 'width=640,height=480,framerate=5/1' # settings for 'video/x-raw'
        enc_params = 'tune=zerolatency quantizer=30 speed-preset=fast' # encoding parameters
        vid_target = f'rtph264pay ! udpsink port=5000 host={self.captain_address[0]} sync=false -e' # target of the video stream (a file)
        # note: the stream port is 5000, NOT the same as the socket.
        return self.ConcatenateGStreamerCommand(vid_source, vid_target, vid_settings, enc_params)


    def Cam2FileStream(self):
        # STEP 2:
        # streaming from camera to file.
        vid_source = 'gst-launch-1.0 v4l2src device=/dev/video0' # here, the source is a camera.
        vid_settings = 'width=640,height=480,framerate=30/1' # settings must be available with the camera hardware, otherwise will fail.
        enc_params = 'tune=zerolatency bitrate=500 speed-preset=superfast' # encoding
        vid_target = 'qtmux ! filesink location=testing/test_pirate.mov sync=false -e' # herein, muxing and filesink are combined.
        # note that the '-e' parameter at the very end makes the gstream susceptible to interrupts.
        return self.ConcatenateGStreamerCommand(vid_source, vid_target, vid_settings, enc_params)


    def Cam2UDPStream(self):
        # STEP 3:
        # stream to the network.
        vid_source = 'gst-launch-1.0 v4l2src device=/dev/video0' # camera
        vid_settings = 'width=640,height=480,framerate=5/1' # vid settings
        enc_params = 'tune=zerolatency bitrate=500 speed-preset=superfast' # encoding settings
        vid_target = f'rtph264pay ! udpsink port=5000 host={self.captain_address[0]} sync=false -e' # target of the video stream (a file)
        # note: the stream port is 5000, NOT the same as the socket.
        return self.ConcatenateGStreamerCommand(vid_source, vid_target, vid_settings, enc_params)

    def Raspivid2File(self):
        # this will record a stream to file using raspivid
        self.recording_counter += 1

        # IMPORTANT: make sure file names are always unique per pirate
        filename = f'recordings/{NowStr()}_rec{self.recording_counter:03.0f}_{self.label}'

        # video settings
        video_settings = " ".join([f'-{key} {value}' for key, value in self.config['video'].items()])

        # concatenate the file streaming command
        video_command = ' '.join(["raspivid -n" \
                                  , video_settings \
                                 , f"-pts {filename}.txt" \
                                 , f"-o {filename}.h264" \
                                 ])

        # mkvmerge -o testing/testvideo.mkv --timecodes 0:testing/testvideo.txt testing/testvideo.h264 && rm testing/testvideo.txt testing/testvideo.h264
        return self.StartStream(stream_pipeline = video_command)

    

    def Stop(self):
        # stop the stream.

        # in case the pirate was idle anyways.
        if self.stream is None:
            return f'{self.label} no stream to stop.'


        # send an interrupt.
        self.stream.send_signal(SG.SIGINT)   # send Ctrl-C signal # TODO: the signal is different on windows.

        try:
            # give the stream a second to finish
            self.stream.wait(self.timeout)
        except SP.TimeoutExpired as expired:
            # ... but kill it if timeout expires.
            # level 1 killer
            self.stream.kill()
            if self.stream.poll() is None:
                # still busy? kill the camera!
                OS.system('pkill raspivid') # TODO: different on windows.
                OS.system('killall gst-launch-1.0') # TODO: different on windows.
                # nasty, right? this is pirates life.

        # shout that the stream stopped.
        self.StdOut('stream stopped.')
        self.busy = False # lower the "busy" flag
        self.stream = None # empty the gun.

        # and report success to the captain.
        return f'{self.label} stream stopped.'


    def матрёшка(self):
        # convert all recordings/timestamps to MKV
        basedir = './recordings'

        # get all files
        files_to_convert = list(sorted([OS.path.splitext(fi)[0] \
                                        for fi in OS.listdir(basedir) \
                                        if OS.path.splitext(fi)[-1] == '.h264' \
                                        ]))

        for fi in files_to_convert:
            filepath = f"{basedir}/{fi}" # no extension

            # sort timestamps # https://stackoverflow.com/a/22281855
            command = f"cat {filepath}.txt " \
                    + """ | awk 'NR<2{print $0;next}{print | "sort -n"}' """ \
                    + f" > {filepath}.pts"
            OS.system(command)


            # prepare command
            command = " && ".join( [ \
                                     f"mkvmerge -o {filepath}.mkv --timecodes 0:{filepath}.txt {filepath}.h264" \
                                   , f"rm {filepath}.h264 {filepath}.txt {filepath}.pts"
                                    ])

            # execute
            OS.system(command)

        return f'{self.label} conversion done!'




#########################
#### Mission Control ####
if __name__=='__main__':
    # this works for testing.
    # You can actually run Pirate and Captain on the same machine (... if it is 64 bit.)
    cam = Pirate()

    # don't forget to quit.
    cam.Quit()




# HOLD: test more encoding options (quality vs. computational load/speed)
# TODO: windows OS.sep
