#!/usr/bin/env python
# -*- coding: utf-8 -*-

__date__ = 20210513
__author__ = "Falk Mielke"


#### THIS IS THE PARROT ####
# it's not dead, it's resting.
"""
Herein I assemble GUI elements, kombined from TK basics.

"""


#### Library Import ####
import tkinter as TK # a basic, but reliable gui library
import tkinter.filedialog as TKF # file dialogs
import tkinter.messagebox as TKM # user interaction
import tkinter.ttk as TTK # separator

colors = { \
              'bg': '#202020' \
            , 'fg': '#FFFFFF' \
            , 'hl': '#303030' \
            }

#### ToolBar class ####
class ToolBar(TK.Frame):
    # A toolbar is a set of adjacent buttons.
    # This class simplifies putting multiple of them next to each other.
    # This class is a subclass of a "Frame", so it can be handled as one.

    def __init__(self, buttons, label = 'menu', separators_after = [], orient_horz = True, **args):
        # invoke the superclass
        super(ToolBar, self).__init__(**args)

        # assemble buttons
        self.buttons = {}


        # choose orientation
        if orient_horz:
            # horizontal: buttons are packed rightwards
            self.button_alignment = TK.RIGHT
            self.button_fill = TK.Y
        else:
            # vertical: buttons are packed below.
            self.button_alignment = TK.BOTTOM
            self.button_fill = TK.X

        pack = dict(side = self.button_alignment \
                    , fill = self.button_fill)

        # label
        self.active = TK.IntVar()
        self.active.set(0)
        self.label_text = TK.StringVar()
        self.label_text.set(label)
        self.label = TK.Radiobutton(master = self \
                              , textvariable = self.label_text \
                              , variable = self.active \
                              , value = 1 \
                              , bg = colors['bg'] \
                              , fg = colors['fg'] \
                              , highlightbackground = colors['hl'] \
                              , highlightcolor = colors['fg'] \
                              , selectcolor = '#008000'
                              , borderwidth = 0 \
                              , height = 4 \
                              , state = TK.DISABLED \
                              )
        self.label.pack(side = TK.LEFT, fill = TK.X)
        # self.active.set(1)

        buttonprops = dict(bg = colors['bg'] \
                           , fg = colors['fg'] \
                           , highlightbackground = colors['hl'] \
                           , highlightcolor = colors['fg'] \
                           , borderwidth = 1 \
                           , height = 4 \
                           # , width = 4 \
                           )

        # iterate the buttons
        for txt, callback in buttons:

            # create a button
            self.buttons[txt] = TK.Button( \
                                          master = self \
                                        , text = txt \
                                        , command = callback \
                                        , **buttonprops \
                                          )
            # ... and pack it.
            self.buttons[txt].pack(**pack)

            if txt in separators_after:
                TK.Label(self, text = '  ', **buttonprops).pack(**pack)
                # TTK.Separator(self \
                #               , orient = 'vertical' if orient_horz else 'horizontal' \
                #               # , padx = 10, pady = 10 \
                #                  ).pack(**pack)


    def Connected(self):
        self.active.set(1)
        for txt, button in self.buttons.items():
            if not txt.lower() in ['ping', 'wake', 'reboot', 'grab', 'del', 'shutdown']:
                button['state'] = 'normal'

    def Disconnected(self):
        self.active.set(0)
        for txt, button in self.buttons.items():
            if not txt.lower() in ['ping', 'wake', 'reboot', 'grab', 'del', 'shutdown']:
                button['state'] = 'disabled'

#### Prompt ####
# prompt user for something
def Prompt(*args, **kwargs):
    return TKM.askquestion(*args, **kwargs) == 'yes'
