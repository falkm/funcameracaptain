#!/usr/bin/env python
# -*- coding: utf-8 -*-

import CaptainObserver as CO



def Connect():
    # Using the same computer for captain and pirate.
    captain_address = ('192.168.128.48', 4005)
    pirates = { \
                'pi1': ('192.168.128.1', 4000) \
                , 'pi2': ('192.168.128.153', 4000) \
                , 'pi3': ('192.168.128.170', 4000) \
                , 'pi4': ('192.168.128.171', 4000) \
               }


    # a captain is created to start work.
    CO.GUICaptain(captain_address, pirates)


if __name__ == "__main__":
    Connect()

